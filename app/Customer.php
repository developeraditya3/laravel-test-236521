<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $primaryKey = 'customerNumber';

    public function orders(){

        return $this->hasMany('App\Order');
    }
    public function payment(){

        return $this->hasOne('App\payments');
    }
}
