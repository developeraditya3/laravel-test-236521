<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected  $primaryKey = 'orderNumber';
    // public function orderDetails()
    // {
    //     return $this->hasOne('App\OrderDetails');
    // }

    public function products()
    {
        return $this->belongsToMany('App\Product','orderdetails','orderNumber','productCode')->withPivot('quantityOrdered','priceEach');
    }

    public function customers()
    {
        return $this->belongsTo('App\Customer','customerNumber','customerNumber');
    }

}
