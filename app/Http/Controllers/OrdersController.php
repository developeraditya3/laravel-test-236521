<?php

namespace App\Http\Controllers;

use App\Order;
use APP\Helpers\APIHelper;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        // echo $id;
        // // your logic goes here.
        // $order = Order::all();
        // dd($order);
        $order = Order::findOrFail($id); 
        $products = $order->products()->get();
        $pArray = [];
        $total = '';
;        foreach($products as $product){
            $prod["product"] = $product->productName;           
            $prod["product_line"] = $product->productLine;
            $prod["unit_price"] = $product->buyprice;
            $prod["qty"] = $product->pivot->quantityOrdered;
            $prod["line_total"] = (int)$product->pivot->quantityOrdered * (float)$product->pivot->priceEach;
            $total = (float)$total + (float)$prod["line_total"];
            $pArray[]= $prod;
        }
        $customer = $order->customers()->first();
        $array = array($order,$products,$customer);
        $res  = array(
            "order_id" => $id,
            "order_date"=>$order->orderDate,
             "status"=>$order->status,
             "order_details" => $pArray,
             "bill_amount" => $total,
             "customer" => array(
                "first_name"=>$customer->contactFirstName,
                "last_name"=>$customer->contactLastName,
                "phone"=>$customer->phone,
                "country_code"=>$customer->country
             )
        );
        // return json_encode($array);
        // $response = APIHelper::createAPIResponse( false, 200, '', $order);
        return response()->json($res);
    }
}
